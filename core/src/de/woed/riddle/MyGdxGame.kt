package de.woed.riddle

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import de.woed.riddle.assets.Assets
import de.woed.riddle.assets.getLazy
import de.woed.riddle.assets.load
import de.woed.riddle.gamescreens.GameScreen
import de.woed.riddle.gamescreens.LockedScreen
import de.woed.riddle.osmanager.OsManager
import de.woed.riddle.ui.customSkin
import de.woed.riddle.ui.screens.BaseScreen
import de.woed.riddle.ui.screens.Clearable
import de.woed.riddle.ui.screens.menu.MainMenuScreen
import ktx.inject.Context
import ktx.scene2d.Scene2DSkin
import kotlin.reflect.full.createInstance

/** The entry point of the game */
class MyGdxGame(private val osManager: OsManager) : BaseGame(), ContextProvider, Clearable {
    override val context by lazy { Context() }
    private val inputMultiplexer by lazy { InputMultiplexer() }
    private val batch by lazy { SpriteBatch() }

    override val assetManager by lazy {
        AssetManager().apply {
            val resolver = InternalFileHandleResolver()
            setLoader(FreeTypeFontGenerator::class.java, FreeTypeFontGeneratorLoader(resolver))
            setLoader(BitmapFont::class.java, ".ttf", FreetypeFontLoader(resolver))
        }
    }

    override var bgColor: Color = Color.BLACK
    private val countDownSoundDuration = 20
    private val countDownSound by assetManager.getLazy(Assets.countdown)

    // Resettable
    private var gameState: GameState? = null
    private var screenIndex = 0
    private var wasCountDownSoundPlayed = false
    private var hasExplosionVibrationHappened = false

    override fun create() {
        registerProviders()
        configureInputMultiplexer()
        loadGlobalAssets()
        loadInitialScreen()
    }

    private fun configureInputMultiplexer() {
        Gdx.input.inputProcessor = inputMultiplexer
        Gdx.input.isCatchBackKey = true
        inputMultiplexer.addProcessor(BackKeyInputProcessor(this))

        // React to swipes left and right
        inputMultiplexer.addProcessor(SimpleDirectionGestureDetector(
                onLeft = { switchRight() },
                onRight = { switchLeft() },
                thresholdX = 2000f
        ))
    }

    private fun loadGlobalAssets() {
        assetManager.load(globalAssets)

        // TODO Remove hack
        assetManager.finishLoading()

        initDefaultSkin()
        registerGlobalAssets()
    }

    override fun loadScreen(screen: BaseScreen) {
        super.loadScreen(screen)

        // TODO Remove hack
        assetManager.finishLoading()
    }

    private fun loadInitialScreen() {
        loadScreen(MainMenuScreen())
    }

    fun startGame() {
        disposeScreen()
        screenIndex = 0
        wasCountDownSoundPlayed = false
        hasExplosionVibrationHappened = false
        val gameState = GameState()
        this.gameState = gameState
        context.register {
            removeProvider(GameState::class.java)
            bindSingleton(gameState)
        }
        val firstScreen = gameState.screenStates.first().item.createInstance()
        loadScreen(firstScreen)
    }

    override fun render() {
        updateTime()
        super.render()
    }

    fun quitToMainMenu() {
        disposeScreen()
        gameState = null
        loadInitialScreen()
    }

    fun canSwitchLeft() = gameState?.screenStates?.getOrNull(screenIndex - 1) != null
    fun canSwitchRight() = gameState?.screenStates?.getOrNull(screenIndex + 1) != null

    fun switchLeft() = switchScreen(screenIndex - 1)
    fun switchRight() = switchScreen(screenIndex + 1)

    private fun switchScreen(index: Int) {
        if (gameState?.isPaused == true) return
        val gameScreenState = gameState?.screenStates?.getOrNull(index) ?: return

        disposeScreen()
        val gameScreen = if (gameScreenState.isUnlocked) {
            gameScreenState.item.createInstance()
        } else LockedScreen(index + 1)
        loadScreen(gameScreen)
        screenIndex = index
    }

    private fun updateTime() {
        gameState?.apply {
            updateTime()

            (screen as? GameScreen)?.showTimeLeft(timeLeft)

            if (timeLeft < countDownSoundDuration && !wasCountDownSoundPlayed) {
                countDownSound.play()
                wasCountDownSoundPlayed = true
            }
//            if (timeLeft < 0 && !wasFailSoundPlayed) {
//                failSound.play()
//                wasFailSoundPlayed = true
//            }

            // Explode when the time is over
            if (timeLeft <= 0 && !hasExplosionVibrationHappened) {
                Gdx.input.vibrate(5000)
                hasExplosionVibrationHappened = true
            }
        }
    }

    private fun initDefaultSkin() {
        val font: BitmapFont = assetManager[Assets.newFont].apply {
            data.setScale(0.5f)
        }

        val customSkin = assetManager[Assets.customSkin]
        Scene2DSkin.defaultSkin = customSkin(customSkin, font)
    }

    private fun registerProviders() = context.register {
        bindSingleton(this@MyGdxGame)
        bindSingleton(osManager)
        bindSingleton(inputMultiplexer)
        bindSingleton<Batch>(batch)
        bindSingleton(assetManager)
        bindSingleton(ShapeRenderer())
    }

    private fun registerGlobalAssets() = context.register {
        bindSingleton(Scene2DSkin.defaultSkin)
        bindSingleton(assetManager[Assets.myBundle])
    }

    /** Disposes [screen] and sets it to null */
    fun disposeScreen() {
        val oldScreen = screen ?: return

        // Includes screen.hide()
        setScreen(null)

        oldScreen.dispose()
    }

    override fun dispose() {
        // Includes screen.hide()
        super.dispose()

        screen?.dispose()
        context.dispose()
    }

    companion object {
        private val globalAssets = listOf(
                Assets.newFont,
                Assets.customSkin,
                Assets.myBundle,
                Assets.success5,
                Assets.gameOver3,
                Assets.countdown
        )
    }
}