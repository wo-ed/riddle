package de.woed.riddle

import de.woed.riddle.localization.MyBundleLine
import de.woed.riddle.localization.getString

// An in-game password (part of a riddle)
const val PASSWORD = "12345678"

// Pad each character so that the password is not spoken like one number by the TTS
val SPOKEN_PASSWORD = PASSWORD.toList().joinToString(" ")

val MORSE_CODE: LongArray = messageToMorseVibrationPatterns(getString(MyBundleLine.MORSE_MESSAGE))