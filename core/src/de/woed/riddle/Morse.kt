package de.woed.riddle

val morseCodeMap = mapOf(
    'A' to ".-",
    'B' to "-...",
    'C' to "-.-.",
    'D' to "-..",
    'E' to ".",
    'F' to "..-.",
    'G' to "--.",
    'H' to "....",
    'I' to "..",
    'J' to ".---",
    'K' to "-.-",
    'L' to ".-..",
    'M' to "--",
    'N' to "-.",
    'O' to "---",
    'P' to ".--.",
    'Q' to "--.-",
    'R' to ".-.",
    'S' to "...",
    'T' to "-",
    'U' to "..-",
    'V' to "...-",
    'W' to ".--",
    'X' to "-..-",
    'Y' to "-.--",
    'Z' to "--..",
    '0' to "-----",
    '1' to ".----",
    '2' to "..---",
    '3' to "...--",
    '4' to "....-",
    '5' to ".....",
    '6' to "-....",
    '7' to "--...",
    '8' to "---..",
    '9' to "----."
)

private const val SHORT_SYMBOL = 250L // Morse "Dit"
private const val LONG_SYMBOL = 6 * SHORT_SYMBOL // Morse "Dah"
private const val CHARACTER_PAUSE = 6 * SHORT_SYMBOL
private const val SYMBOL_PAUSE = SHORT_SYMBOL // Pause after each signal
private const val WORD_PAUSE = 14 * SHORT_SYMBOL
private const val END_PAUSE = WORD_PAUSE
private const val INITIAL_PAUSE = 0L

fun messageToMorseVibrationPatterns(message: String): LongArray {
    val words = message.split(' ')

    val messageVibrationPattern = words.flatMap { word: String ->
        val wordVibrationPattern = word.flatMap { char ->
            val symbols: String = morseCodeMap[char]!!

            val charVibrationPattern = symbols.flatMap {
                listOf(SYMBOL_PAUSE, getSymbolDuration(it)!!)
            }
            listOf(CHARACTER_PAUSE) + charVibrationPattern.drop(1)
        }
        listOf(WORD_PAUSE) + wordVibrationPattern.drop(1)
    }

    return  (listOf(INITIAL_PAUSE) + messageVibrationPattern.drop(1) + END_PAUSE).toLongArray()
}

private fun getSymbolDuration(symbol: Char): Long? = when (symbol) {
    '.' -> SHORT_SYMBOL
    '-' -> LONG_SYMBOL
    else -> null
}