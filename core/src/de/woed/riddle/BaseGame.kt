package de.woed.riddle

import com.badlogic.gdx.Game
import com.badlogic.gdx.assets.AssetManager
import de.woed.riddle.ui.screens.BaseScreen
import de.woed.riddle.ui.screens.Clearable

abstract class BaseGame : Game(), Clearable {
    protected abstract val assetManager: AssetManager

    protected var nextScreen: BaseScreen? = null

    override fun render() {
        clearColor()
        assetManager.update()
        checkNextScreen()
        super.render()
    }

    protected open fun checkNextScreen() {
        if (nextScreen?.isLoaded() == true) {
            showNextScreen()
        }
    }

    protected fun showNextScreen() {
        setScreen(nextScreen)
        nextScreen?.create()
        nextScreen = null
    }

    open fun loadScreen(screen: BaseScreen) {
        screen.load()
        nextScreen = screen
    }
}