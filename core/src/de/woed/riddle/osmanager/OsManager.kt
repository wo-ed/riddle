package de.woed.riddle.osmanager

/** Manages OS-specific device features */
interface OsManager {
    val faceDetector: CameraDetector<List<Face>>?
    val labelDetector: CameraDetector<List<Label>>?
    val textDetector: CameraDetector<String>?
    val textSpeaker: TextSpeaker?
    val notifier: Notifier?
    val lightSensor: LightSensor?
    val flashlight: Flashlight?
    val frontSpeaker: FrontSpeaker?
}