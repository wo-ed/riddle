package de.woed.riddle.osmanager

interface LightSensor {
    val lux: Float?
}