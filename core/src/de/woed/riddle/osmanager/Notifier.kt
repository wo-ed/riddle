package de.woed.riddle.osmanager

interface Notifier {
    fun showNotification(title: String, text: String, isHidden: Boolean = false)
    fun hideNotification()
}