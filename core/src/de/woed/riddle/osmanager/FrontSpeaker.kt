package de.woed.riddle.osmanager

interface FrontSpeaker {
    fun setFrontSpeakerOn(isFrontSpeakerOn: Boolean)
}