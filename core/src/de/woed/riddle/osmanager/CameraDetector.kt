package de.woed.riddle.osmanager

interface CameraDetector<T> {
    fun setUpCamera(front: Boolean)
    fun startCamera()
    fun stopCamera()
    fun pauseCamera()
    fun resumeCamera()
    fun showCameraView()
    fun hideCameraView()
    fun startDetection(callback: (T) -> Unit)
    fun stopDetection()
}