package de.woed.riddle.osmanager

class Face(
        val smilingProbability: Float,
        val leftEyeOpenProbability: Float,
        val rightEyeOpenProbability: Float,
        val headEulerAngleY: Float,
        val headEulerAngleZ: Float
) {

    override fun toString() = "(smiling: $smilingProbability, " +
            "left eye: $leftEyeOpenProbability, " +
            "right eye: $rightEyeOpenProbability)"

    val isSmiling get() = smilingProbability >= SMILING

    companion object {
        private const val SMILING = 0.9
    }
}