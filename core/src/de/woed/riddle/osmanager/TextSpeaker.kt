package de.woed.riddle.osmanager

interface TextSpeaker {
    fun speak(text: String)
    fun stop()
}