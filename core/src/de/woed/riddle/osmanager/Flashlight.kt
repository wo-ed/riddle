package de.woed.riddle.osmanager

interface Flashlight {
    fun prepare()
    fun turnLightOn()
    fun turnLightOff()
    fun close()
}