package de.woed.riddle.osmanager

class Label(val label: String) {
    override fun toString() = label
}