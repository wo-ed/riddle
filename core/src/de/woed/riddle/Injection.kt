package de.woed.riddle

import com.badlogic.gdx.Gdx
import ktx.inject.Context

interface ContextProvider {
    val context: Context
}

inline fun <reified T : Any> inject() = (Gdx.app.applicationListener as ContextProvider).context.inject<T>()
inline fun <reified T : Any> lazyInjection() = lazy { inject<T>() }