package de.woed.riddle

import com.badlogic.gdx.input.GestureDetector
import kotlin.math.abs

class SimpleDirectionGestureDetector(
        onLeft: (() -> Unit)? = null,
        onRight: (() -> Unit)? = null,
        onUp: (() -> Unit)? = null,
        onDown: (() -> Unit)? = null,
        thresholdX: Float = 0f,
        thresholdY: Float = 0f
) : GestureDetector(object : GestureAdapter() {

    override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean {
        if (abs(velocityX) > abs(velocityY)) {
            if (velocityX > thresholdX) {
                onRight?.invoke()
            } else if (velocityX < -thresholdX) {
                onLeft?.invoke()
            }
        } else {
            if (velocityY > thresholdY) {
                onDown?.invoke()
            } else if (velocityY < -thresholdY) {
                onUp?.invoke()
            }
        }
        return super.fling(velocityX, velocityY, button)
    }
})