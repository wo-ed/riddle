package de.woed.riddle.assets

import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.I18NBundle
import ktx.assets.assetDescriptor

object Assets {

    // Folders
    const val MENU = "menu"
    const val SKINS = "$MENU/skins"
    const val FONTS = "$MENU/fonts"
    const val TEXTURES = "textures"
    const val SOUNDS = "sounds"
    const val MAPS = "maps"
    const val BUNDLES = "bundles"

    // Menus
    val background = assetDescriptor<Texture>("$TEXTURES/background.png")
    val background2 = assetDescriptor<Texture>("$TEXTURES/background2.jpg")
    val click = assetDescriptor<Sound>("$SOUNDS/171697__nenadsimic__menu-selection-click.ogg")
    val notification = assetDescriptor<Sound>("$SOUNDS/242502__gabrielaraujo__pop-up-notification.ogg")
    val comicSkin = assetDescriptor<Skin>("$SKINS/comic/comic-ui.json")
    val customSkin = assetDescriptor<Skin>("$SKINS/custom/custom.json")

    private const val KOMIKA_DISPLAY_TTF = "$FONTS/komika-display.ttf"
    val comicFont = assetDescriptor<BitmapFont>(
            KOMIKA_DISPLAY_TTF,
            FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                fontFileName = KOMIKA_DISPLAY_TTF
                fontParameters.size = 32
                fontParameters.color = Color.WHITE
            }
    )

    private const val NEW_FONT = "$FONTS/orkney-medium.ttf"
    val newFont = assetDescriptor<BitmapFont>(
            NEW_FONT,
            FreetypeFontLoader.FreeTypeFontLoaderParameter().apply {
                fontFileName = NEW_FONT
                fontParameters.size = 64
                fontParameters.color = Color.WHITE
            }
    )

    // Face games
    val stare = assetDescriptor<Texture>("$TEXTURES/stare.png")
    val tunnel = assetDescriptor<Texture>("$TEXTURES/tunnel.png")

    // Game modes
    val win = assetDescriptor<Sound>("$SOUNDS/258142__tuudurt__level-win.ogg")
    val win2 = assetDescriptor<Sound>("$SOUNDS/275104__tuudurt__piglevelwin2.ogg")
    val win3 = assetDescriptor<Sound>("$SOUNDS/342218__littlerainyseasons__good-end.ogg")
    val gameOver = assetDescriptor<Sound>("$SOUNDS/133283__fins__game-over.ogg")
    val gameOver2 = assetDescriptor<Sound>("$SOUNDS/135831__davidbain__end-game-fail.ogg")
    val gameOver3 = assetDescriptor<Sound>("$SOUNDS/371205__mouse85224__level-failed.ogg")
    val success = assetDescriptor<Sound>("$SOUNDS/171670__fins__success-2.ogg")
    val success2 = assetDescriptor<Sound>("$SOUNDS/171671__fins__success-1.ogg")
    val success3 = assetDescriptor<Sound>("$SOUNDS/242501__gabrielaraujo__powerup-success.ogg")
    val success4 = assetDescriptor<Sound>("$SOUNDS/325805__wagna__collect.ogg")
    val success5 = assetDescriptor<Sound>("$SOUNDS/362445__tuudurt__positive-response.ogg")
    val fail = assetDescriptor<Sound>("$SOUNDS/242503__gabrielaraujo__failure-wrong-action.ogg")
    val ringtone = assetDescriptor<Music>("$SOUNDS/276612__mickleness__ringtone-tizzy.ogg")
    val countdown = assetDescriptor<Music>("$SOUNDS/165089__russintheus__countdown-boom.ogg")
    val dial = assetDescriptor<Sound>("$SOUNDS/dial.ogg")
    val dialFail = assetDescriptor<Sound>("$SOUNDS/negative_beep.ogg")

    // Localization
    val myBundle = assetDescriptor<I18NBundle>("$BUNDLES/MyBundle")
}