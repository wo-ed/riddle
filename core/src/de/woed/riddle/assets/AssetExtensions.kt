package de.woed.riddle.assets

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import de.woed.riddle.inject
import ktx.actors.onClick
import ktx.assets.ignore
import ktx.assets.unloadSafely

fun <T> AssetManager.getOrNull(descriptor: AssetDescriptor<T>): T? = try {
    get(descriptor)
} catch (e: Exception) {
    e.ignore()
    null
}

fun AssetManager.load(assets: Iterable<AssetDescriptor<*>>) = assets.forEach { load(it) }
fun AssetManager.load(vararg assets: AssetDescriptor<*>) = assets.forEach { load(it) }
fun Iterable<AssetDescriptor<*>>.load(assetManager: AssetManager) = forEach { assetManager.load(it) }

fun AssetManager.unloadSafely(descriptor: AssetDescriptor<*>) = unloadSafely(descriptor.fileName)
fun AssetManager.unloadSafely(assets: Iterable<AssetDescriptor<*>>) = assets.forEach { unloadSafely(it) }
fun AssetManager.unloadSafely(vararg assets: AssetDescriptor<*>) = assets.forEach { unloadSafely(it) }
fun Iterable<AssetDescriptor<*>>.unloadSafely(assetManager: AssetManager) = forEach { assetManager.unloadSafely(it) }

fun AssetManager.isLoaded(assets: Iterable<AssetDescriptor<*>>) = assets.all { isLoaded(it.fileName, it.type) }
fun AssetManager.isLoaded(vararg assets: AssetDescriptor<*>) = assets.all { isLoaded(it.fileName, it.type) }
fun Iterable<AssetDescriptor<*>>.isLoaded(assetManager: AssetManager) = all { assetManager.isLoaded(it) }

fun AssetManager.finishLoadingAssets(assets: Iterable<AssetDescriptor<*>>) = assets.forEach { finishLoadingAsset(it) }
fun AssetManager.finishLoadingAssets(vararg assets: AssetDescriptor<*>) = assets.forEach { finishLoadingAsset(it) }
fun Iterable<AssetDescriptor<*>>.finishLoadingAssets(assetManager: AssetManager) = forEach { assetManager.finishLoadingAsset(it) }

inline fun Actor.onClickWithSound(crossinline listener: () -> Unit): ClickListener {
    return this.onClick {
        val sound: Sound? = inject<AssetManager>().getOrNull(Assets.click)
        sound?.play()
        listener()
    }
}

fun <T> AssetManager.getLazy(descriptor: AssetDescriptor<T>, init: (T.() -> Unit)? = null) = lazy {
    get(descriptor).apply { init?.invoke(this) }
}