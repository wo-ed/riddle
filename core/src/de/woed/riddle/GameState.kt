package de.woed.riddle

import com.badlogic.gdx.Gdx
import de.woed.riddle.gamescreens.*
import de.woed.riddle.gamescreens.camera.*
import de.woed.riddle.gamescreens.test.SensorTestScreen
import de.woed.riddle.gamescreens.test.TextDetectionTestScreen
import kotlin.reflect.KClass

class GameState {
    var timeLeft = 3600f
        private set

    var isPaused = false

    val screenStates get() = unlockables.mapNotNull { it as? UnlockableScreen }

    private val unlockables = listOf(
        UnlockableScreen(NotificationScreen::class, isUnlocked = true),
        UnlockableScreen(HiddenMessageScreen::class, isUnlocked = true),
        UnlockableScreen(CowerScreen::class, isUnlocked = true),
        UnlockableScreen(SensorTestScreen::class, isUnlocked = true),
        UnlockableScreen(PhoneCallScreen::class, isUnlocked = true),
        UnlockableScreen(MorseScreen::class, isUnlocked = true),
        UnlockableScreen(PinScreen::class, isUnlocked = true),
        UnlockableScreen(DarkScreen::class, isUnlocked = true),
        UnlockableScreen(FlickerScreen::class, isUnlocked = true),
        UnlockableScreen(ShakeScreen::class, isUnlocked = true),
        UnlockableScreen(PaperDetectionScreen::class, isUnlocked = true),
        UnlockableScreen(TextDetectionTestScreen::class, isUnlocked = true),
        UnlockableScreen(BehindYouScreen::class),
        UnlockableScreen(RunScreen::class),
        Unlockable("item1"),
        Unlockable("item2"),
        UnlockableScreen(BlindScreen::class),
        UnlockableScreen(HideScreen::class),
        UnlockableScreen(SmileScreen::class)
    )

    init {
        unlockables.forEach { it.isUnlocked = true }
    }

    fun <T> setFinished(item: T, value: Boolean) {
        getItemState(item).isFinished = value
    }

    fun <T> getItemState(item: T) = unlockables.single { it.item == item }

    fun unlockNext(): Boolean {
        val next = unlockables.firstOrNull { !it.isUnlocked }
        next?.isUnlocked = true
        return next != null
    }

    fun updateTime() {
        if (!isPaused) {
            timeLeft -= Gdx.graphics.deltaTime
        }
    }
}

class UnlockableScreen(
    unlockable: KClass<out GameScreen>,
    isUnlocked: Boolean = false,
    isFinished: Boolean = false
) : Unlockable<KClass<out GameScreen>>(
    unlockable,
    isUnlocked,
    isFinished
)

open class Unlockable<T>(
    var item: T,
    var isUnlocked: Boolean = false,
    var isFinished: Boolean = false
)