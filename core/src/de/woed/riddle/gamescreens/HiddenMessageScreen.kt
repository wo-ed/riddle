package de.woed.riddle.gamescreens

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import de.woed.riddle.Compass
import de.woed.riddle.Gyroscope
import de.woed.riddle.PADDING
import de.woed.riddle.localization.MyBundleLine
import de.woed.riddle.localization.getString
import de.woed.riddle.lux
import ktx.actors.alpha
import ktx.scene2d.label
import kotlin.math.min

class HiddenMessageScreen : GameScreen() {

    private lateinit var messageLabel: Label
    private lateinit var messageLabel2: Label

    override fun initialize() {
        super.initialize()

        with(gameControlsTable) {
            defaults().pad(PADDING)
            messageLabel = label(getString(MyBundleLine.LOCKED)) {
                it.growX()
                setWrap(true)
                setAlignment(Align.center)
            }
            row()
            messageLabel2 = label(getString(MyBundleLine.LOCKED)) {
                it.growX()
                setWrap(true)
                setAlignment(Align.center)
            }
        }
    }

    override fun drawGame(delta: Float) {
        super.drawGame(delta)

        val cappedLux = min(lux ?: 0f, MAX_LUX)
        messageLabel.alpha = 1 - cappedLux / MAX_LUX
        //messageLabel2.alpha = cappedLux / MAX_LUX

        messageLabel2.setFontScale(Compass.pitch / 10, Compass.roll / 10)
    }

    companion object {
        private const val MAX_LUX = 50f
    }
}