package de.woed.riddle.gamescreens

import de.woed.riddle.localization.MyBundleLine.LIGHT_FLICKERING
import de.woed.riddle.lux

class FlickerScreen : MessageScreen(LIGHT_FLICKERING) {
    private var count = 0
    private var lastOn: Boolean? = null
        set (value) {
            if (value != null && field != value) {
                count++
            }
            field = value
        }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)

        val lux = lux ?: return

        when {
            isLightOff(lux) && wasLightOnOrNull() -> lastOn = false
            isLightOn(lux) && wasLightOffOrNull() -> lastOn = true
        }

        if (count >= TARGET_COUNT) {
            finish()
        }
    }

    private fun wasLightOff() = lastOn == false
    private fun wasLightOffOrNull() = lastOn == null || wasLightOff()

    private fun wasLightOn() = lastOn == true
    private fun wasLightOnOrNull() = lastOn == null || wasLightOn()

    private fun isLightOn(lux: Float) = lux > THRESHOLD
    private fun isLightOff(lux: Float) = lux <= THRESHOLD

    companion object {
        private const val THRESHOLD = 0f
        private const val TARGET_COUNT = 6
    }
}