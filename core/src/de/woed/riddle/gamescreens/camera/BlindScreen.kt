package de.woed.riddle.gamescreens.camera

import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.*
import de.woed.riddle.osmanager.Face
import de.woed.riddle.osmanager.OsManager

class BlindScreen : FaceDetectionScreen(message = BLINDED) {

    override val detector = inject<OsManager>().faceDetector

    override fun onProcessFrame(result: List<Face>) {
        if (!isPaused && result.any {
                    it.leftEyeOpenProbability <= 0.3 && it.rightEyeOpenProbability <= 0.3
                }) {
            finish()
        }
    }
}