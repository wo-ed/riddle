package de.woed.riddle.gamescreens.camera

import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.PAPER
import de.woed.riddle.osmanager.Label
import de.woed.riddle.osmanager.OsManager

class PaperDetectionScreen : LabelDetectionScreen(
        usesFrontCamera = false,
        message = PAPER
) {
    override val detector = inject<OsManager>().labelDetector

    override fun onProcessFrame(result: List<Label>) {
        if (!isPaused && result.map { it.label }.contains("Paper")) {
            finish()
        }
    }
}