package de.woed.riddle.gamescreens.camera

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import de.woed.riddle.DEFAULT_VIBRATION_TIME
import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.HIDE
import de.woed.riddle.localization.MyBundleLine.SHOW_YOURSELF
import de.woed.riddle.localization.setText
import de.woed.riddle.osmanager.Face
import de.woed.riddle.osmanager.OsManager

class HideScreen : FaceDetectionScreen(message = SHOW_YOURSELF) {
    override val detector = inject<OsManager>().faceDetector

    private var isHideEnabled: Boolean = false
        set(value) {
            field = value
            setStyle(value)
        }

    override fun initialize() {
        super.initialize()
        isHideEnabled = false
    }

    fun setStyle(isHideEnabled: Boolean) {
        bgColor = if (isHideEnabled) {
            messageLabel.setText(HIDE)
            setTextColor(Color.WHITE)

            Color.BLACK
        } else {
            messageLabel.setText(SHOW_YOURSELF)
            setTextColor(Color.BLACK)

            Color.WHITE
        }
    }

    private fun setTextColor(color: Color) {
        messageLabel.color = color
        timeLabel.color = color
        leftButton.label.color = color
        rightButton.label.color = color
    }

    override fun onProcessFrame(result: List<Face>) {
        when {
            isPaused -> return
            !isHideEnabled && result.any() -> {
                Gdx.input.vibrate(DEFAULT_VIBRATION_TIME)
                isHideEnabled = true
            }
            isHideEnabled && !result.any() -> finish()
        }
    }
}