package de.woed.riddle.gamescreens.camera

import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.BEHIND_YOU
import de.woed.riddle.osmanager.Face
import de.woed.riddle.osmanager.OsManager

class BehindYouScreen : FaceDetectionScreen(message = BEHIND_YOU) {

    override val detector = inject<OsManager>().faceDetector

    override fun onProcessFrame(result: List<Face>) {
        if (!isPaused && result.count() >= 2) {
            finish()
        }
    }
}