package de.woed.riddle.gamescreens.camera

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import de.woed.riddle.PADDING
import de.woed.riddle.defaultUiStage
import de.woed.riddle.gamescreens.GameScreen
import de.woed.riddle.halfHeightUiStage
import de.woed.riddle.localization.label
import de.woed.riddle.osmanager.CameraDetector
import de.woed.riddle.osmanager.Face
import ktx.i18n.BundleLine

typealias FaceDetectionScreen = DetectionScreen<List<Face>>
typealias LabelDetectionScreen = DetectionScreen<List<de.woed.riddle.osmanager.Label>>
typealias TextDetectionScreen = DetectionScreen<String>

abstract class DetectionScreen<T>(
        gameStage: Stage? = null,
        uiStage: Stage = halfHeightUiStage(),
        isDrawBordersEnabled: Boolean = false,
        usesFrontCamera: Boolean = true,
        private val message: BundleLine? = null
) : GameScreen(gameStage, uiStage, isDrawBordersEnabled) {

    abstract val detector: CameraDetector<T>?

    protected lateinit var messageLabel: Label

    protected var usesFrontCamera = usesFrontCamera
        set(value) {
            field = value
            detector?.setUpCamera(value)
        }

    protected abstract fun onProcessFrame(result: T)

    override fun initialize() {
        super.initialize()

        if (message == null) return

        with(gameControlsTable) {
            defaults().pad(PADDING)
            messageLabel = label(message) {
                it.growX()
                setWrap(true)
                setAlignment(Align.center)
            }
        }
    }

    override fun show() {
        super.show()
        detector?.setUpCamera(usesFrontCamera)
        detector?.startCamera()
        detector?.showCameraView()
        detector?.startDetection(::onProcessFrame)
    }

    override fun hide() {
        super.hide()
        detector?.hideCameraView()
        detector?.stopDetection()
        detector?.stopCamera()
    }

    override fun resume() {
        super.resume()
        detector?.resumeCamera()
    }

    override fun pause() {
        super.pause()
        detector?.pauseCamera()
    }

    override fun finish() {
        super.finish()
        detector?.stopCamera()
    }
}