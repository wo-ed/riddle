package de.woed.riddle.gamescreens.camera

import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.LAUGH_MESSAGE
import de.woed.riddle.osmanager.Face
import de.woed.riddle.osmanager.OsManager

class SmileScreen : FaceDetectionScreen(message = LAUGH_MESSAGE) {

    override val detector = inject<OsManager>().faceDetector

    override fun onProcessFrame(result: List<Face>) {
        if (!isPaused && result.all { it.isSmiling }) {
            finish()
        }
    }
}