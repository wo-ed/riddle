package de.woed.riddle.gamescreens

import com.badlogic.gdx.utils.Align
import de.woed.riddle.PADDING
import de.woed.riddle.assets.Assets
import de.woed.riddle.morseCodeMap
import ktx.scene2d.label
import ktx.scene2d.scrollPane
import ktx.scene2d.table

class MorseScreen : GameScreen() {

    override fun initialize() {
        super.initialize()

        with(gameControlsTable) {
            scrollPane {
                table {
                    defaults().pad(PADDING)
                    morseCodeMap.entries.forEachIndexed { index, (char, sign) ->
                        label("$char")
                        label(sign) {
                            setAlignment(Align.right)
                        }
                        if (index % 2 == 1) {
                            row()
                        }
                    }
                }
            }
        }
    }
}
