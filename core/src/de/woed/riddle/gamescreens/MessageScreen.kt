package de.woed.riddle.gamescreens

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import de.woed.riddle.PADDING
import de.woed.riddle.localization.MyBundleLine.LOCKED
import de.woed.riddle.localization.MyBundleLine.RUN
import de.woed.riddle.localization.getString
import ktx.i18n.BundleLine
import ktx.scene2d.label

class LockedScreen(number: Int) : MessageScreen("#$number ${getString(LOCKED)}")
class RunScreen : MessageScreen(RUN)

/** A screen that displays a simple message. */
open class MessageScreen(private val message: String) : GameScreen() {
    constructor(bundleLine: BundleLine) : this(getString(bundleLine))

    protected lateinit var messageLabel: Label

    override fun initialize() {
        super.initialize()

        with(gameControlsTable) {
            defaults().pad(PADDING)
            messageLabel = label(message) {
                it.growX()
                setWrap(true)
                setAlignment(Align.center)
            }
        }
    }
}

/** A [MessageScreen] that finishes immediately. */
open class FinishingMessageScreen(message: String) : MessageScreen(message) {
    constructor(bundleLine: BundleLine) : this(getString(bundleLine))

    override fun show() {
        super.show()
        if (!isFinished) {
            isFinished = true
            gameState.unlockNext()
        }
    }
}