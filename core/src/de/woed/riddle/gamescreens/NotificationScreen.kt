package de.woed.riddle.gamescreens

import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.RECEIVED_MESSAGE
import de.woed.riddle.osmanager.OsManager

class NotificationScreen : MessageScreen(RECEIVED_MESSAGE) {
    private val notifier = inject<OsManager>().notifier

    override fun show() {
        super.show()
        notifier?.showNotification("???", "XYZ", isHidden = true)
    }

    override fun hide() {
        super.hide()
        notifier?.hideNotification()
    }
}