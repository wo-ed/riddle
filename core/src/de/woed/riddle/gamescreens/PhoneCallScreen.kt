package de.woed.riddle.gamescreens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Timer
import de.woed.riddle.*
import de.woed.riddle.assets.Assets
import de.woed.riddle.assets.getLazy
import de.woed.riddle.localization.MyBundleLine.*
import de.woed.riddle.localization.label
import de.woed.riddle.localization.textButton
import de.woed.riddle.osmanager.OsManager
import ktx.actors.onClick

class PhoneCallScreen : GameScreen() {
    private val textSpeaker = inject<OsManager>().textSpeaker
    private val frontSpeaker = inject<OsManager>().frontSpeaker

    override val assetDescriptors = listOf(Assets.ringtone)
    private val ringtone by assetManager.getLazy(Assets.ringtone)

    private val speakerTask = gdxTimerTask {
        textSpeaker?.speak(SPOKEN_PASSWORD)
    }

    override var isPaused
        get() = super.isPaused
        set(value) {
            super.isPaused = value
            if (value) {
                cancelEverything()
            } else {
                startCall()
            }
        }

    override fun initialize() {
        super.initialize()

        with(gameControlsTable) {
            defaults().pad(PADDING)
            label(INCOMING_CALL)
            row()
            label(UNKNOWN_CALLER)
            row()
            textButton(ACCEPT) {
                onClick {
                    acceptCall()
                    isVisible = false
                }
            }
        }
    }

    private fun acceptCall() {
        Gdx.input.cancelVibrate()
        ringtone.stop()
        frontSpeaker?.setFrontSpeakerOn(true)
        Timer.schedule(speakerTask, VOICE_DELAY)
    }

    private fun cancelEverything() {
        Gdx.input.cancelVibrate()
        ringtone.stop()
        textSpeaker?.stop()
        speakerTask.cancel()
        frontSpeaker?.setFrontSpeakerOn(false)
    }

    private fun startCall() {
        Gdx.input.vibrate(MORSE_CODE, 0)
        ringtone.play()
        ringtone.isLooping = true
    }

    override fun show() {
        super.show()
        startCall()
    }

    override fun resume() {
        super.resume()
        startCall()
    }

    override fun pause() {
        super.pause()
        cancelEverything()
    }

    override fun hide() {
        super.hide()
        cancelEverything()
    }

    companion object {
        private const val VOICE_DELAY = 3f
    }
}