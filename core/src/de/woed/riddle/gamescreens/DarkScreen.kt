package de.woed.riddle.gamescreens

import de.woed.riddle.localization.MyBundleLine.LIGHT_OUT
import de.woed.riddle.lux

class DarkScreen : MessageScreen(LIGHT_OUT) {

    override fun updateGame(delta: Float) {
        super.updateGame(delta)

        lux?.let {
            if (it <= THRESHOLD) {
                finish()
            }
        }
    }

    companion object {
        private const val THRESHOLD = 0f
    }
}