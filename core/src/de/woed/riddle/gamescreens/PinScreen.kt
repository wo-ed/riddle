package de.woed.riddle.gamescreens

import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.utils.Align
import de.woed.riddle.PADDING
import de.woed.riddle.PASSWORD
import de.woed.riddle.assets.Assets
import de.woed.riddle.assets.getLazy
import de.woed.riddle.localization.MyBundleLine.DEL
import de.woed.riddle.localization.MyBundleLine.OK
import de.woed.riddle.localization.textButton
import ktx.actors.onClick
import ktx.scene2d.textButton
import ktx.scene2d.textField

class PinScreen : GameScreen() {
    private lateinit var pinField: TextField

    override val assetDescriptors = listOf(Assets.dial, Assets.dialFail)

    private val dialSound by assetManager.getLazy(Assets.dial)
    private val dialFailSound by assetManager.getLazy(Assets.dialFail)

    override fun initialize() {
        super.initialize()

        with(gameControlsTable) {
            defaults().pad(PADDING)
            pinField = textField {
                isDisabled = true
                isPasswordMode = true
                setPasswordCharacter('*')
                setAlignment(Align.center)
                it.colspan(3).fillX().height(TEXTFIELD_HEIGHT)
            }
            row()
            for (digit in 1..9) {
                textButton("$digit") {
                    it.size(BUTTON_SIZE)
                    onClick { addDigitToPinField(digit) }
                }
                if ((digit - 1) % 3 == 2) {
                    row()
                }
            }

            textButton(DEL) {
                it.size(BUTTON_SIZE)
                onClick { deleteLastDigitFromPinfield() }
            }
            textButton("0") {
                it.size(BUTTON_SIZE)
                onClick { addDigitToPinField(0) }
            }
            textButton(OK) {
                it.size(BUTTON_SIZE)
                onClick { confirmPin() }
            }
        }
    }

    private fun deleteLastDigitFromPinfield() {
        if (pinField.text.isEmpty()) {
            dialFailSound.stop()
            dialFailSound.play()
        } else {
            dialSound.play()
            pinField.text = pinField.text.dropLast(1)
        }
    }

    private fun addDigitToPinField(number: Int) {
        if (pinField.text.length < MAX_PIN_LENGTH) {
            pinField.text += number
            dialSound.play()
        } else {
            dialFailSound.stop()
            dialFailSound.play()
        }
    }

    private fun confirmPin() {
        if (pinField.text.trim() == PASSWORD) {
            finish()
        } else {
            pinField.text = ""
            dialFailSound.stop()
            dialFailSound.play()
        }
    }

    companion object {
        private const val MAX_PIN_LENGTH = 8
        private const val BUTTON_SIZE = 100f
        private const val TEXTFIELD_HEIGHT = BUTTON_SIZE
    }
}