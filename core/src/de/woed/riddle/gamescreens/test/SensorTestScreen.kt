package de.woed.riddle.gamescreens.test

import com.badlogic.gdx.utils.Align
import de.woed.riddle.*
import de.woed.riddle.gamescreens.MessageScreen
import ktx.actors.txt

class SensorTestScreen : MessageScreen("") {
    private var frameCounter = 0

    override fun initialize() {
        super.initialize()
        messageLabel.setAlignment(Align.left)
    }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)

        if (frameCounter++ % 60 == 0) {
            messageLabel.txt = """
            GyroX: %.4f
            GyroY: %.4f
            GyroZ: %.4f

            Pitch: %.4f
            Roll: %.4f
            Azimuth: %.4f

            AccX: %.4f
            AccY: %.4f
            AccZ: %.4f

            Lux: %.4f
            """.trimIndent().format(Gyroscope.x, Gyroscope.y, Gyroscope.z, Compass.pitch, Compass.azimuth, Compass.roll, Accelerometer.x, Accelerometer.y, Accelerometer.z, lux)
        }
    }
}