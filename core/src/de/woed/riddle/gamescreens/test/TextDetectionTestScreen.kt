package de.woed.riddle.gamescreens.test

import de.woed.riddle.gamescreens.camera.TextDetectionScreen
import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.SCAN_TEXT
import de.woed.riddle.osmanager.OsManager
import ktx.actors.txt

class TextDetectionTestScreen : TextDetectionScreen(
        usesFrontCamera = false,
        message = SCAN_TEXT
) {
    override val detector = inject<OsManager>().textDetector

    private var scannedText = ""

    override fun onProcessFrame(result: String) {
        scannedText = result
    }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)
        messageLabel.txt = scannedText
    }
}