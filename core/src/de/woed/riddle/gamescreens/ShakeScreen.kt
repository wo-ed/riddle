package de.woed.riddle.gamescreens

import com.badlogic.gdx.Gdx
import de.woed.riddle.*
import de.woed.riddle.localization.MyBundleLine.SHAKE
import de.woed.riddle.osmanager.OsManager
import kotlin.math.sqrt

class ShakeScreen : MessageScreen(SHAKE) {
    private val lightManager = inject<OsManager>().flashlight

    private var lastShakeTime = 0L
    private var shakeCount = 0
    private var frameCounter = 0f

    private var isLightOn: Boolean = false
        set(value) {
            field = value
            if (value) {
                lightManager?.turnLightOn()
            } else {
                lightManager?.turnLightOff()
            }
        }

    override var isPaused: Boolean
        get() = super.isPaused
        set(value) {
            super.isPaused = value
            if (value) {
                isLightOn = false
            }
        }

    override fun updateGame(delta: Float) {
        super.updateGame(delta)

        if (isFinished) return

        updateLight()
        detectShake()
    }

    private fun updateLight() {
        frameCounter++
        if (frameCounter % 20 == 0f) {
            isLightOn = !isLightOn
        }
    }

    private fun detectShake() {
        val curTime = System.currentTimeMillis()
        if (curTime - lastShakeTime > MIN_TIME_BETWEEN_SHAKES) {

            val (x, y, z) = Accelerometer
            val acceleration = sqrt(x * x + y * y + z * z) - GRAVITY_EARTH

            if (acceleration > SHAKE_THRESHOLD) {
                lastShakeTime = curTime
                onShakeDetected()
            }
        }
    }

    private fun onShakeDetected() {
        Gdx.input.vibrate(DEFAULT_VIBRATION_TIME)
        shakeCount++
        if (shakeCount >= TARGET_SHAKE_COUNT) {
            finish()
        }
    }

    override fun show() {
        super.show()
        lightManager?.prepare()
    }

    override fun hide() {
        super.hide()
        lightManager?.close()
    }

    override fun finish() {
        super.finish()
        isLightOn = true
    }

    companion object {
        private const val MIN_TIME_BETWEEN_SHAKES = 100L
        private const val SHAKE_THRESHOLD = 15
        private const val GRAVITY_EARTH = 9.80665f
        private const val TARGET_SHAKE_COUNT = 10
    }
}