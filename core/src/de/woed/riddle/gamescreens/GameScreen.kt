package de.woed.riddle.gamescreens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.badlogic.gdx.utils.Align
import de.woed.riddle.*
import de.woed.riddle.assets.Assets
import de.woed.riddle.localization.MyBundleLine.QUIT
import de.woed.riddle.localization.MyBundleLine.RESUME
import de.woed.riddle.localization.textButton
import de.woed.riddle.ui.screens.StageScreen
import ktx.actors.onClick
import ktx.actors.txt
import ktx.scene2d.*

abstract class GameScreen(
        gameStage: Stage? = null,
        override val uiStage: Stage = defaultUiStage(),
        var isDrawBordersEnabled: Boolean = false
) : StageScreen(
        gameStage = gameStage,
        inputMultiplexer = inject()
) {
    protected val gameState: GameState = inject()
    protected val myGdxGame: MyGdxGame = inject()

    protected open var finishSound: Sound = assetManager[Assets.success5]

    protected var isFinished: Boolean
        get() = gameState.getItemState(this::class).isFinished
        set(value) {
            gameState.setFinished(this::class, value)
        }

    /** Contains [gameControlsTable] and the bottom control elements */
    private lateinit var gameRootTable: KTableWidget

    // Should contain the controls for each individual game
    protected lateinit var gameControlsTable: KTableWidget

    protected lateinit var timeLabel: Label
    protected lateinit var pauseDialog: KWindow
    protected lateinit var leftButton: KTextButton
    protected lateinit var rightButton: KTextButton

    // Pause menu
    open var isPaused
        get() = gameState.isPaused
        set(value) {
            gameState.isPaused = value
            pauseDialog.isVisible = value
            setGameControlsTouchable(!value)
        }

    override fun initialize() {
        super.initialize()
        initBaseLayout()
        showTimeLeft(gameState.timeLeft)
    }

    private fun initBaseLayout() {
        gameRootTable = table root@{
            setFillParent(true)
            gameControlsTable = table {
                it.grow()
            }
            row()
            table { bottomCell ->
                bottomCell.growX().padTop(PADDING).padBottom(PADDING)
                leftButton = textButton("<", style = "no_image") {
                    it.width(Value.percentWidth(1 / 3f, this@root))
                    onClick { myGdxGame.switchLeft() }
                }
                timeLabel = label("") {
                    it.width(Value.percentWidth(1 / 3f, this@root))
                    setAlignment(Align.center)
                }
                rightButton = textButton(">", style = "no_image") {
                    it.width(Value.percentWidth(1 / 3f, this@root))
                    onClick { myGdxGame.switchRight() }
                }
            }
        }

        // TODO Title?
        pauseDialog = window("") {
            setFillParent(true)
            defaults().pad(PADDING)
            isModal = true
            isMovable = false
            isVisible = isPaused

            textButton(RESUME) {
                it.fillX()
                onClick { togglePaused() }
            }
            row()
            textButton(QUIT) {
                it.fillX()
                onClick { myGdxGame.quitToMainMenu() }
            }
        }

        uiStage.addActor(gameRootTable)
        uiStage.addActor(pauseDialog)
    }

    protected open fun finish() {
        if (!isFinished) {
            gameState.unlockNext()
            isFinished = true
            Gdx.input.vibrate(DEFAULT_VIBRATION_TIME)
            finishSound.play()
        }
    }

    fun showTimeLeft(timeLeft: Float) {
        val minutes = (timeLeft / 60f).toInt()
        val seconds = (timeLeft % 60f).toInt()
        timeLabel.txt = String.format("%02d:%02d", minutes, seconds)
    }

    private fun setGameControlsTouchable(isTouchable: Boolean) {
        gameRootTable.touchable =
                if (isTouchable) Touchable.enabled
                else Touchable.disabled
    }

    protected open fun setLabelStyle(style: Label.LabelStyle) {
        timeLabel.style = style
    }

    override fun onBackPressed() {
        super.onBackPressed()
        togglePaused()
    }

    // Pause menu
    private fun togglePaused() {
        isPaused = !isPaused
    }

    /** Renders the game AND the control elements on top. */
    override fun render(delta: Float) {
        if (!isPaused) {
            updateGame(delta)
        }

        drawBackground()
        drawGame(delta)
        renderUiStage(delta)
    }

    /** Updates the actual game, NOT the control elements.
     *  This method is NOT called when the game is paused. */
    protected open fun updateGame(delta: Float) {
        leftButton.isVisible = myGdxGame.canSwitchLeft()
        rightButton.isVisible = myGdxGame.canSwitchRight()
        gameStage?.act(delta)
    }

    /** Draws the actual game, NOT the control elements.
     * This method is also called when the game is paused. */
    protected open fun drawGame(delta: Float) {
        gameStage?.viewport?.apply()
        gameStage?.draw()
    }
}