package de.woed.riddle

import com.badlogic.gdx.Gdx
import de.woed.riddle.osmanager.OsManager

object Gyroscope {
    val x get() = Gdx.input.gyroscopeX
    val y get() = Gdx.input.gyroscopeY
    val z get() = Gdx.input.gyroscopeZ
    operator fun component1() = Gdx.input.gyroscopeX
    operator fun component2() = Gdx.input.gyroscopeY
    operator fun component3() = Gdx.input.gyroscopeZ
}

object Compass {
    val pitch get() = Gdx.input.pitch
    val roll get() = Gdx.input.roll
    val azimuth get() = Gdx.input.azimuth
    operator fun component1() = Gdx.input.pitch
    operator fun component2() = Gdx.input.roll
    operator fun component3() = Gdx.input.azimuth
}

object Accelerometer {
    val x get() = Gdx.input.accelerometerX
    val y get() = Gdx.input.accelerometerY
    val z get() = Gdx.input.accelerometerZ
    operator fun component1() = Gdx.input.accelerometerX
    operator fun component2() = Gdx.input.accelerometerY
    operator fun component3() = Gdx.input.accelerometerZ
}

val lux get() = inject<OsManager>().lightSensor?.lux