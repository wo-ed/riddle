package de.woed.riddle.ui

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import de.woed.riddle.ninePatchDrawable
import ktx.style.*

fun customSkin(skin: Skin, defaultFont: BitmapFont) = skin.apply {
    textButton {
        up = ninePatchDrawable("button_default_up", 20, 20, 20, 20)
        down = ninePatchDrawable("button_default_down", 20, 20, 20, 20)
        font = defaultFont
        fontColor = Color.WHITE
        downFontColor = Color.BLACK
    }

    textButton("no_image") {
        font = defaultFont
        fontColor = Color.WHITE
        downFontColor = Color.GRAY
    }

    label {
        font = defaultFont
        fontColor = Color.WHITE
    }

    textField {
        font = defaultFont
        fontColor = Color.BLACK
        background = ninePatchDrawable("textfield_default", 20, 20, 20, 20)
    }

    window {
        titleFont = defaultFont
        titleFontColor = Color.WHITE
        background = ninePatchDrawable("window_default", 20, 20, 20, 20)
    }

    scrollPane {
        vScrollKnob = skin["scrollpane_vscrollknob_default"]
    }
}