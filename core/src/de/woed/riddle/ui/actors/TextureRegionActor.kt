package de.woed.riddle.ui.actors

import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion

open class TextureRegionActor() : AnimatedActor<TextureRegion>() {
    constructor(region: TextureRegion?) : this() {
        this.region = region
        synchronizeSize()
    }

    constructor(animation: Animation<TextureRegion>?) : this() {
        this.animation = animation
        synchronizeSize()
    }

    var region: TextureRegion?
        get() = image
        set(value) {
            image = value
        }

    var flipX = false
    var flipY = false

    fun synchronizeSize() {
        width = region?.regionWidth?.toFloat() ?: 0f
        height = region?.regionHeight?.toFloat() ?: 0f
        originX = width / 2f
        originY = height / 2f
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        super.draw(batch, parentAlpha)
        region?.let {
            it.flip(flipX, flipY)
            batch?.draw(it, x, y, originX, originY, width, height, scaleX, scaleY, rotation)
            it.flip(flipX, flipY)
        }
    }
}