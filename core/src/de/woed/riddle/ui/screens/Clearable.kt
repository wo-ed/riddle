package de.woed.riddle.ui.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20

interface Clearable {
    var bgColor: Color

    fun clearColor() {
        clearColor(bgColor)
    }
}

fun clearColor(color: Color) {
    Gdx.gl.glClearColor(color.r, color.g, color.b, color.a)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
}