package de.woed.riddle.ui.screens.menu

import com.badlogic.gdx.Gdx
import de.woed.riddle.MyGdxGame
import de.woed.riddle.PADDING
import de.woed.riddle.inject
import de.woed.riddle.localization.MyBundleLine.*
import de.woed.riddle.localization.textButton
import ktx.actors.onClick
import ktx.scene2d.table

class MainMenuScreen : MenuScreen() {
    private val myGdxGame: MyGdxGame = inject()

    override fun initialize() {
        super.initialize()
        initLayout()
    }

    private fun initLayout() {
        val table = table {
            setFillParent(true)
            defaults().pad(PADDING)
            textButton(START) {
                it.fillX()
                onClick { myGdxGame.startGame() }
            }
            row()
            textButton(MINIGAMES) {
                it.fillX()
            }
            row()
            textButton(SETTINGS) {
                it.fillX()
            }
        }

        uiStage.addActor(table)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Gdx.app.exit()
    }
}