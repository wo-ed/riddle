package de.woed.riddle.ui.screens.menu

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.FillViewport

import de.woed.riddle.assets.Assets
import de.woed.riddle.assets.getLazy
import de.woed.riddle.defaultUiStage
import de.woed.riddle.inject
import de.woed.riddle.render
import de.woed.riddle.ui.screens.StageScreen

abstract class MenuScreen : StageScreen(inputMultiplexer = inject()) {
    override val uiStage = defaultUiStage()
    protected val batch: Batch = inject()
    private val textureViewport = FillViewport(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())

    override val assetDescriptors = listOf(Assets.background)

    private val texture: Texture by assetManager.getLazy(Assets.background) {
        setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
    }

    override fun render(delta: Float) {
        clearColor()
        renderBackground()
        renderUiStage(delta)
    }

    private fun renderBackground() {
        // Calculate size to fit the screen
        // Keep aspect ratio
        val aspectRatio = texture.width.toFloat() / texture.height
        val width = aspectRatio * textureViewport.worldHeight
        val height = textureViewport.worldHeight

        textureViewport.apply()
        batch.render(textureViewport.camera.combined) {
            draw(
                    texture,
                    textureViewport.camera.position.x - width / 2f,
                    textureViewport.camera.position.y - height / 2f,
                    width,
                    height
            )
        }
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        textureViewport.update(width, height, true)
    }
}