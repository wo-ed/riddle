package de.woed.riddle.ui.screens

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.viewport.Viewport
import de.woed.riddle.assets.finishLoadingAssets
import de.woed.riddle.assets.isLoaded
import de.woed.riddle.assets.load
import de.woed.riddle.assets.unloadSafely
import de.woed.riddle.drawBackground
import de.woed.riddle.inject

abstract class BaseScreen : ScreenAdapter(), Clearable {
    protected val assetManager: AssetManager = inject()

    protected open val assetDescriptors: Iterable<AssetDescriptor<*>>? = null
    protected open val viewport: Viewport? = null

    protected val shapeRenderer: ShapeRenderer = inject()
    protected var isDrawBarsEnabled = false
    protected var barColor: Color = Color.BLACK
    override var bgColor: Color = Color.BLACK

    var isInitialized = false
        private set

    protected open fun initialize() {}

    fun create() {
        initialize()
        isInitialized = true
    }

    protected open fun drawBackground() {
        val viewport = this.viewport
        if (viewport != null && isDrawBarsEnabled) {
            drawBackground(viewport)
        } else {
            clearColor()
        }
    }

    protected fun drawBackground(viewport: Viewport) {
        clearColor(barColor)
        shapeRenderer.drawBackground(viewport, bgColor)
    }

    override fun render(delta: Float) {
        super.render(delta)
        drawBackground()
    }

    open fun onBackPressed() {}

    open fun load() = assetDescriptors?.load(assetManager)
    open fun isLoaded() = assetDescriptors == null || assetDescriptors?.isLoaded(assetManager) == true
    open fun finishLoading() = assetDescriptors?.finishLoadingAssets(assetManager)

    override fun dispose() {
        super.dispose()
        assetDescriptors?.unloadSafely(assetManager)
    }
}