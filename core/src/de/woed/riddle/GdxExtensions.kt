package de.woed.riddle

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable
import com.badlogic.gdx.utils.Timer
import com.badlogic.gdx.utils.viewport.Viewport

fun ShapeRenderer.drawBackground(
        viewport: Viewport,
        col1: Color,
        col2: Color = col1,
        col3: Color = col1,
        col4: Color = col3
) {
    val camera = viewport.camera
    val zoom = (camera as? OrthographicCamera)?.zoom ?: 1f
    val width = viewport.worldWidth * zoom
    val height = viewport.worldHeight * zoom

    viewport.apply()
    render(camera.combined, ShapeRenderer.ShapeType.Filled) {
        rect(
                camera.position.x - width / 2f,
                camera.position.y - height / 2f,
                width, height,
                col1, col2, col3, col4
        )
    }
}

fun ShapeRenderer.rect(rect: Rectangle, degrees: Float = 0f) = rect(
        rect.x, rect.y,
        rect.width / 2f, rect.height / 2f,
        rect.width, rect.height,
        1f, 1f,
        degrees
)

inline fun ShapeRenderer.render(
        projectionMatrix: Matrix4? = null,
        shapeType: ShapeRenderer.ShapeType = ShapeRenderer.ShapeType.Line,
        draw: ShapeRenderer.() -> Unit
) {
    projectionMatrix?.let { this.projectionMatrix = it }
    setAutoShapeType(true)
    begin(shapeType)
    draw()
    end()
}

/** Sets the projection matrix to be used by this [ShapeRenderer].
 * Avoids flushing by checking if the values are already equal. */
fun ShapeRenderer.setProjectionMatrixIfNotEqual(projectionMatrix: Matrix4) {
    if (this.projectionMatrix.values?.contentEquals(projectionMatrix.values) == false) {
        this.projectionMatrix = projectionMatrix
    }
}

/** Sets the projection matrix to be used by this [Batch].
 * Avoids flushing by checking if the values are already equal. */
fun Batch.setProjectionMatrixIfNotEqual(projectionMatrix: Matrix4) {
    if (this.projectionMatrix.values?.contentEquals(projectionMatrix.values) == false) {
        this.projectionMatrix = projectionMatrix
    }
}

inline fun Batch.render(projectionMatrix: Matrix4? = null, draw: Batch.() -> Unit) {
    projectionMatrix?.let { this.projectionMatrix = it }
    begin()
    draw()
    end()
}

fun Batch.beginSafely() {
    if (!isDrawing) {
        begin()
    }
}

fun Batch.endSafely() {
    if (isDrawing) {
        end()
    }
}

fun TextureAtlas.findAnimation(
        name: String,
        frameDuration: Float,
        playMode: Animation.PlayMode = Animation.PlayMode.NORMAL
): Animation<TextureAtlas.AtlasRegion>? {

    val regions = findRegions(name)
    return if (regions.size > 0) {
        Animation(frameDuration, regions, playMode)
    } else null
}

fun Sprite.setWidth(width: Float, keepAspectRatio: Boolean = false) {
    val height = if (keepAspectRatio) width / this.width * this.height else this.height
    setSize(width, height)
}

fun Sprite.setHeight(height: Float, keepAspectRatio: Boolean = false) {
    val width = if (keepAspectRatio) height / this.height * this.width else this.width
    setSize(width, height)
}

fun Skin.ninePatchDrawable(regionName: String, left: Int, right: Int, top: Int, bottom: Int) =
        NinePatchDrawable(NinePatch(getRegion(regionName), left, right, top, bottom))

inline fun gdxTimerTask(crossinline action: Timer.Task.() -> Unit): Timer.Task = object : Timer.Task() {
    override fun run() = action()
}