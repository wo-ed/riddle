package de.woed.riddle.localization
import ktx.i18n.BundleLine

/** Generated from android/assets/bundles/MyBundle.properties file. */
enum class MyBundleLine : BundleLine {
    START,
    MINIGAMES,
    SETTINGS,
    RESUME,
    QUIT,
    LAUGH_MESSAGE,
    OK,
    DEL,
    LOCKED,
    ACCEPT,
    RECEIVED_MESSAGE,
    INCOMING_CALL,
    UNKNOWN_CALLER,
    SHAKE,
    RUN,
    BEHIND_YOU,
    LIGHT_OUT,
    LIGHT_FLICKERING,
    SCAN_TEXT,
    PAPER,
    BLINDED,
    SHOW_YOURSELF,
    HIDE,
    DUCK,
    MORSE_MESSAGE,
}
