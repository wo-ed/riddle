package de.woed.riddle.localization

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import ktx.i18n.BundleLine
import ktx.scene2d.*

inline fun <S> KWidget<S>.textButton(
        text: BundleLine,
        style: String = defaultStyle,
        skin: Skin = Scene2DSkin.defaultSkin,
        init: KTextButton.(S) -> Unit = {}) = textButton(getString(text), style, skin, init)

inline fun <S> KWidget<S>.label(
        text: BundleLine,
        style: String = defaultStyle,
        skin: Skin = Scene2DSkin.defaultSkin,
        init: (@Scene2dDsl Label).(S) -> Unit = {}) = label(getString(text), style, skin, init)

fun Label.setText(text: BundleLine) = setText(getString(text))