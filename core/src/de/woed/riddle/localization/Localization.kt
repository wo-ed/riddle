package de.woed.riddle.localization

import com.badlogic.gdx.utils.I18NBundle
import de.woed.riddle.inject
import ktx.i18n.BundleLine
import ktx.i18n.get

fun getString(key: MyBundleLine) = inject<I18NBundle>()[key]

fun getString(key: BundleLine) = when (key) {
    is MyBundleLine -> getString(key)
    else -> throw RuntimeException()
}