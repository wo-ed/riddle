package de.woed.riddle.packer

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.headless.HeadlessApplication
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration
import de.woed.riddle.assets.Assets.SKINS

object PackerLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val assetPacker = AssetPacker()
        val config = HeadlessApplicationConfiguration()
        HeadlessApplication(assetPacker, config)
    }
}

class AssetPacker : ApplicationAdapter() {
    override fun create() {
        super.create()
        packSubfoldersToAtlases(input = "../../resources/skins", outputParent = SKINS)
        Gdx.app.exit()
    }
}