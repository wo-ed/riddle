package de.woed.riddle.packer

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.tools.texturepacker.TexturePacker
import java.io.File.separator

fun packSubfoldersToAtlases(input: String, outputParent: String, settings: TexturePacker.Settings = defaultTexturePackerSettings(), progress: TexturePacker.ProgressListener? = null) {
    val subfolders = Gdx.files.local(input).list().filter { it.isDirectory }
    subfolders.forEach { packFolderToAtlas(it, outputParent, settings, progress) }
}

fun packFolderToAtlas(input: FileHandle, outputParent: String, settings: TexturePacker.Settings = defaultTexturePackerSettings(), progress: TexturePacker.ProgressListener? = null) {
    val output = "$outputParent$separator${input.name()}"

    if (progress != null) {
        TexturePacker.process(settings, input.path(), output, input.name(), progress)
    } else {
        TexturePacker.process(settings, input.path(), output, input.name())
    }
}

fun defaultTexturePackerSettings() = TexturePacker.Settings().apply {
    maxWidth = 2048
    maxHeight = 2048
    combineSubdirectories = true
    duplicatePadding = true
}