package de.woed.riddle

import android.os.Build
import android.os.Bundle
import android.view.View
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.google.firebase.FirebaseApp
import de.woed.riddle.osmanager.AndroidOsManager
import de.woed.riddle.osmanager.NotificationChannels.createNotificationChannels
import kotlinx.android.synthetic.main.activity_launcher.*

class AndroidLauncher : AndroidApplication() {
    private val osManager by lazy { AndroidOsManager(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

        FirebaseApp.initializeApp(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannels()
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            val perms = arrayOf("android.permission.RECORD_AUDIO", "android.permission.CAMERA")
            val permsRequestCode = 200
            requestPermissions(perms, permsRequestCode)
        }

        val config = AndroidApplicationConfiguration().apply {
            useWakelock = true
            useGyroscope = true
            //useImmersiveMode = true
        }
        val gameView = initializeForView(MyGdxGame(osManager), config)
        gameViewContainer.addView(gameView)

        osManager.onActivityCreate()
    }

    override fun onStart() {
        super.onStart()
        osManager.onActivityStart()
    }

    override fun onResume() {
        super.onResume()
        osManager.onActivityResume()
    }

    override fun onPause() {
        super.onPause()
        osManager.onActivityPause()
    }

    override fun onStop() {
        super.onStop()
        osManager.onActivityStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        osManager.onActivityDestroy()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }

    // Enables regular immersive mode.
    // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
    // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    private fun hideSystemUI() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) return

        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private fun showSystemUI() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) return

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }
}