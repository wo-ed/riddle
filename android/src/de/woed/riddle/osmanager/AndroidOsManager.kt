package de.woed.riddle.osmanager

import android.app.Activity
import de.woed.riddle.osmanager.camera.*
import de.woed.riddle.osmanager.sensors.AndroidLightSensorAdapter

class AndroidOsManager(private val activity: Activity) : OsManager {

    private lateinit var cameraManager: CameraManager
    override lateinit var faceDetector: FirebaseFaceDetector private set
    override lateinit var labelDetector: FirebaseLabelDetector private set
    override lateinit var textDetector: FirebaseTextDetector private set
    override lateinit var textSpeaker: AndroidTextSpeaker private set
    override lateinit var notifier: Notifier private set
    override lateinit var lightSensor: AndroidLightSensorAdapter private set
    override lateinit var flashlight: Flashlight private set
    override lateinit var frontSpeaker: FrontSpeaker private set

    fun onActivityCreate() {
        cameraManager = CameraManager(activity)
        faceDetector = FirebaseFaceDetector(cameraManager)
        labelDetector = FirebaseLabelDetector(cameraManager)
        textDetector = FirebaseTextDetector(cameraManager)
        textSpeaker = AndroidTextSpeaker(activity)
        notifier = AndroidNotifier(activity)
        lightSensor = AndroidLightSensorAdapter(activity)
        flashlight = AndroidLightManager(cameraManager)
        frontSpeaker = AndroidFrontSpeaker(activity)
    }

    fun onActivityStart() {
        cameraManager.onActivityStart()
    }

    fun onActivityResume() {
        lightSensor.onActivityResume()
    }

    fun onActivityPause() {
        lightSensor.onActivityPause()
    }

    fun onActivityStop() {
        cameraManager.onActivityStop()
    }

    fun onActivityDestroy() {
        textSpeaker.onActivityDestroy()
    }
}