package de.woed.riddle.osmanager.sensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

// TODO Register only when the screen is used
// TODO Error handling
open class AndroidSensorAdapter(context: Context, sensorType: Int) : SensorEventListener {
    private val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager?
    private val sensor = sensorManager?.getDefaultSensor(sensorType)

    var values: FloatArray? = null
        private set

    override fun onSensorChanged(event: SensorEvent?) {
        values = event?.values
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    fun onActivityResume() {
        sensorManager?.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME)
    }

    fun onActivityPause() {
        sensorManager?.unregisterListener(this)
    }
}