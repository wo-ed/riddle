package de.woed.riddle.osmanager.sensors

import android.content.Context
import android.hardware.Sensor
import de.woed.riddle.osmanager.LightSensor

class AndroidLightSensorAdapter(context: Context) : AndroidSensorAdapter(context, Sensor.TYPE_LIGHT), LightSensor {
    override val lux: Float? get() = values?.first()
}