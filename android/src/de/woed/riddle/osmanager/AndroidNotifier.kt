package de.woed.riddle.osmanager

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import de.woed.riddle.R

class AndroidNotifier(
    private val context: Context
) : Notifier {

    override fun showNotification(title: String, text: String, isHidden: Boolean) {
        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannels.secrets;
            NotificationCompat.Builder(context, channel.id).setPriority(channel.importance)
        } else {
            NotificationCompat.Builder(context, "") // TODO
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        }.apply {
            // TODO
            if (isHidden) {
                setVibrate(null)
                setSound(null)
            } else {
                setDefaults(Notification.DEFAULT_ALL)
            }

            setSmallIcon(R.drawable.ic_fingerprint_black_24dp)
            setContentTitle(title)
            setContentText(text)
        }

        with(NotificationManagerCompat.from(context)) {
            val notification = builder.build().also {
                // Make the notification sticky
                it.flags =
                    it.flags or (Notification.FLAG_NO_CLEAR or Notification.FLAG_ONGOING_EVENT)
            }
            notify(NOTIFICATION_SECRET, notification)
        }
    }

    override fun hideNotification() {
        with(NotificationManagerCompat.from(context)) {
            cancel(NOTIFICATION_SECRET)
        }
    }

    companion object {
        const val NOTIFICATION_SECRET = 1
    }
}

@RequiresApi(Build.VERSION_CODES.O)
object NotificationChannels {

    val secrets =
        NotificationChannel("SECRETS", "Secrets", NotificationManager.IMPORTANCE_HIGH)
            .apply {
                description = "Show a secret (important part of the game)"
            }

    private val allChannels = listOf(secrets)

    fun Context.createNotificationChannels() {
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        allChannels.forEach { manager.createNotificationChannel(it) }
    }
}