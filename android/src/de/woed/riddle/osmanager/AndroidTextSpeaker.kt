package de.woed.riddle.osmanager

import android.content.Context
import android.os.Build
import android.speech.tts.TextToSpeech

class AndroidTextSpeaker(context: Context) : TextSpeaker {
    private val tts = TextToSpeech(context) {
        // TODO Error handling
    }

    override fun speak(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null)
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null)
        }
    }

    override fun stop() {
        tts.stop()
    }

    fun onActivityDestroy() {
        tts.shutdown()
    }
}