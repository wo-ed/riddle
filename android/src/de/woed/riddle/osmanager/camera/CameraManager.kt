package de.woed.riddle.osmanager.camera

import android.app.Activity
import android.hardware.Camera
import android.util.SparseIntArray
import android.view.Surface
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import de.woed.riddle.R
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.selector.back
import io.fotoapparat.selector.front
import io.fotoapparat.selector.torch
import io.fotoapparat.util.FrameProcessor
import io.fotoapparat.view.CameraView

class CameraManager(private val activity: Activity) {
    private val cameraView: CameraView = activity.findViewById(R.id.cameraView)
    private val constraintLayout: ConstraintLayout = activity.findViewById(R.id.constraintLayout)

    private val fotoapparat = Fotoapparat
            .with(activity)
            .into(cameraView)
            .previewScaleType(ScaleType.CenterCrop)
            .lensPosition(front())
            .build()

    var frameProcessor: FrameProcessor? = null

    private val defaultConfig = CameraConfiguration(
            frameProcessor = { frameProcessor?.invoke(it) }
    )

    private val cameraInfo = Camera.CameraInfo()
    private var cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT

    private var isCameraEnabled = false
        set(value) {
            field = value
            if (value) {
                fotoapparat.start()
            } else {
                fotoapparat.stop()
            }
        }

    fun getRotationCompensation(): Int {
        // Get the device's current rotation relative to its "native" orientation.
        // Then, from the ORIENTATIONS table, look up the angle the image must be
        // rotated to compensate for the device's rotation.
        val deviceRotation = activity.windowManager.defaultDisplay.rotation
        var rotationCompensation = ORIENTATIONS.get(deviceRotation)

        // On most devices, the sensor orientation is 90 degrees, but for some
        // devices it is 270 degrees. For devices with a sensor orientation of
        // 270, rotate the image an additional 180 ((270 + 270) % 360) degrees.
        /* val cameraManager = activity.getSystemService(CAMERA_SERVICE) as CameraManager
        val sensorOrientation = cameraManager
                .getCameraCharacteristics("1")
                .get(CameraCharacteristics.SENSOR_ORIENTATION)!! */
        Camera.getCameraInfo(cameraId, cameraInfo)
        val sensorOrientation = cameraInfo.orientation

        rotationCompensation = (rotationCompensation + sensorOrientation + 270) % 360

        // Return the corresponding FirebaseVisionImageMetadata rotation value.
        return when (rotationCompensation) {
            0 -> FirebaseVisionImageMetadata.ROTATION_0
            90 -> FirebaseVisionImageMetadata.ROTATION_90
            180 -> FirebaseVisionImageMetadata.ROTATION_180
            270 -> FirebaseVisionImageMetadata.ROTATION_270
            else -> FirebaseVisionImageMetadata.ROTATION_0
        }
    }

    fun startCamera() {
        isCameraEnabled = true
    }

    fun stopCamera() {
        isCameraEnabled = false
    }

    fun resumeCamera() {
        isCameraEnabled = true
    }

    fun pauseCamera() {
        isCameraEnabled = false
    }

    fun setUpCamera(front: Boolean, configuration: CameraConfiguration = defaultConfig) {
        val lens = if (front) front() else back()
        cameraId = if (front) Camera.CameraInfo.CAMERA_FACING_FRONT else Camera.CameraInfo.CAMERA_FACING_BACK
        fotoapparat.switchTo(lens, configuration)
    }

    fun showCameraView() {
        activity.runOnUiThread {

            // Half cameraView half gameViewContainer
            with(ConstraintSet()) {
                clone(constraintLayout)
                connect(R.id.cameraView, ConstraintSet.BOTTOM, R.id.gameViewContainer, ConstraintSet.TOP)
                connect(R.id.gameViewContainer, ConstraintSet.TOP, R.id.cameraView, ConstraintSet.BOTTOM)
                applyTo(constraintLayout)
            }
        }
    }

    fun hideCameraView() {
        activity.runOnUiThread {

            // Both, cameraView and gameViewContainer are fullscreen (gameViewContainer is on top)
            // The cameraView is not hidden so that the flashlight can still work
            with(ConstraintSet()) {
                clone(constraintLayout)
                connect(R.id.cameraView, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                connect(R.id.gameViewContainer, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)
                applyTo(constraintLayout)
            }
        }
    }

    fun onActivityStart() {
        if (isCameraEnabled) {
            fotoapparat.start()
        }
    }

    fun onActivityStop() {
        fotoapparat.stop()
    }

    fun turnLightOn(oldConfig: CameraConfiguration = defaultConfig) {
        val newConfig = oldConfig.copy(flashMode = torch())
        fotoapparat.updateConfiguration(newConfig)
    }

    fun resetConfig() {
        fotoapparat.updateConfiguration(defaultConfig)
    }

    companion object {
        private val ORIENTATIONS = SparseIntArray().apply {
            append(Surface.ROTATION_0, 90)
            append(Surface.ROTATION_90, 0)
            append(Surface.ROTATION_180, 270)
            append(Surface.ROTATION_270, 180)
        }
    }
}