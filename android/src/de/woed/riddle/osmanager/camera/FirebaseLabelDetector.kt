package de.woed.riddle.osmanager.camera

import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceImageLabelerOptions
import de.woed.riddle.osmanager.Label
import io.fotoapparat.preview.Frame

// TODO close?
class FirebaseLabelDetector(cameraManager: CameraManager) : AndroidCameraDetector<List<Label>, List<FirebaseVisionImageLabel>>(cameraManager) {
    private val visionLabelDetector = FirebaseVision.getInstance().getOnDeviceImageLabeler(
            FirebaseVisionOnDeviceImageLabelerOptions.Builder().setConfidenceThreshold(0.6f).build()
    )

    override fun detectInFrame(frame: Frame): Task<List<FirebaseVisionImageLabel>> {
        val image = cameraManager.extractFirebaseVisionImage(frame)
        return visionLabelDetector.processImage(image)
    }

    override fun convert(u: List<FirebaseVisionImageLabel>) = u.map { Label(it.text) }
}