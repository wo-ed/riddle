package de.woed.riddle.osmanager.camera

import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.text.FirebaseVisionText
import io.fotoapparat.preview.Frame

// TODO close?
class FirebaseTextDetector(cameraManager: CameraManager) : AndroidCameraDetector<String, FirebaseVisionText>(cameraManager) {
    private val visionLabelDetector = FirebaseVision.getInstance().onDeviceTextRecognizer

    override fun detectInFrame(frame: Frame): Task<FirebaseVisionText> {
        val image = cameraManager.extractFirebaseVisionImage(frame)
        return visionLabelDetector.processImage(image)
    }

    override fun convert(u: FirebaseVisionText) = u.text!!
}