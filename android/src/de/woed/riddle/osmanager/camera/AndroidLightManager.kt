package de.woed.riddle.osmanager.camera

import de.woed.riddle.osmanager.Flashlight

// TODO Combine with CameraDetector interface?
class AndroidLightManager(private val cameraManager: CameraManager) : Flashlight {
    override fun prepare() {
        cameraManager.setUpCamera(front = false)
        cameraManager.startCamera()
    }

    override fun turnLightOn() {
        cameraManager.turnLightOn()
    }

    override fun turnLightOff() {
        cameraManager.resetConfig()
    }

    override fun close() {
        cameraManager.stopCamera()
    }
}