package de.woed.riddle.osmanager.camera

import android.util.Log
import com.google.android.gms.tasks.Task
import de.woed.riddle.osmanager.CameraDetector
import io.fotoapparat.preview.Frame

// TODO Rename T and U
abstract class AndroidCameraDetector<T, U>(
    protected val cameraManager: CameraManager
) : CameraDetector<T> {

    private var callback: ((T) -> Unit)? = null
    private var isBusy = false

    override fun setUpCamera(front: Boolean) = cameraManager.setUpCamera(front)
    override fun startCamera() = cameraManager.startCamera()
    override fun stopCamera() = cameraManager.stopCamera()
    override fun pauseCamera() = cameraManager.pauseCamera()
    override fun resumeCamera() = cameraManager.resumeCamera()
    override fun showCameraView() = cameraManager.showCameraView()
    override fun hideCameraView() = cameraManager.hideCameraView()

    override fun startDetection(callback: (T) -> Unit) {
        cameraManager.frameProcessor = this::processFrame
        this.callback = callback
        isBusy = false
    }

    override fun stopDetection() {
        cameraManager.frameProcessor = null
        callback = null
        isBusy = false
    }

    private fun processFrame(frame: Frame) {
        if (isBusy) {
            return
        }

        isBusy = true
        detectInFrame(frame).addOnCompleteListener { task ->
            task.result?.let { processResult(it) }
            task.exception?.let { Log.e(TAG, "Error while detecting in camera frame", it) }
        }
    }

    private fun processResult(result: U) {
        val t = convert(result)
        callback?.invoke(t)
        isBusy = false
        Log.d(TAG, "$t")
    }

    protected abstract fun detectInFrame(frame: Frame): Task<U>
    protected abstract fun convert(u: U): T

    companion object {
        private const val TAG = "AndroidCameraDetector"
    }
}