package de.woed.riddle.osmanager.camera

import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import de.woed.riddle.osmanager.Face
import io.fotoapparat.preview.Frame

// TODO close?
class FirebaseFaceDetector(cameraManager: CameraManager) : AndroidCameraDetector<List<Face>, List<FirebaseVisionFace>>(cameraManager) {
    private val options = FirebaseVisionFaceDetectorOptions.Builder()
            .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
            .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
            .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
            .setMinFaceSize(0.1f)
            .setContourMode(FirebaseVisionFaceDetectorOptions.NO_CONTOURS)
            .build()

    private val visionFaceDetector = FirebaseVision.getInstance().getVisionFaceDetector(options)

    override fun detectInFrame(frame: Frame): Task<List<FirebaseVisionFace>> {
        val image = cameraManager.extractFirebaseVisionImage(frame)
        return visionFaceDetector.detectInImage(image)
    }

    override fun convert(u: List<FirebaseVisionFace>): List<Face> = u.map {
        Face(
                it.smilingProbability,
                it.leftEyeOpenProbability,
                it.rightEyeOpenProbability,
                it.headEulerAngleY,
                it.headEulerAngleZ
        )
    }
}