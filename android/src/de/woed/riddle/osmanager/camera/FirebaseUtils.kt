package de.woed.riddle.osmanager.camera

import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import io.fotoapparat.preview.Frame

fun CameraManager.extractFirebaseVisionImage(frame: Frame): FirebaseVisionImage {
    val rotation: Int = getRotationCompensation()

    val metadata = FirebaseVisionImageMetadata.Builder()
            .setWidth(frame.size.width)
            .setHeight(frame.size.height)
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .setRotation(rotation)
            .build()

    return FirebaseVisionImage.fromByteArray(frame.image, metadata)
}