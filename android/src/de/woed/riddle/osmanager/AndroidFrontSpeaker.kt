package de.woed.riddle.osmanager

import android.content.Context
import android.media.AudioManager

class AndroidFrontSpeaker(context: Context) : FrontSpeaker {
    private val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager?

    override fun setFrontSpeakerOn(isFrontSpeakerOn: Boolean) {
        audioManager?.mode = if (isFrontSpeakerOn) AudioManager.MODE_IN_COMMUNICATION else AudioManager.MODE_NORMAL
        audioManager?.isSpeakerphoneOn = !isFrontSpeakerOn
    }
}