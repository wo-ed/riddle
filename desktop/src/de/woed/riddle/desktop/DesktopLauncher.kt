package de.woed.riddle.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import de.woed.riddle.MyGdxGame

object DesktopLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration().apply {
            width = 432
            height = 768
        }
        val osManager = DesktopOsManager()
        val myGdxGame = MyGdxGame(osManager)
        LwjglApplication(myGdxGame, config)
    }
}