package de.woed.riddle.desktop

import de.woed.riddle.osmanager.*

class DesktopOsManager : OsManager {
    override val faceDetector: CameraDetector<List<Face>>? = null
    override val labelDetector: CameraDetector<List<Label>>? = null
    override val textDetector: CameraDetector<String>? = null
    override val textSpeaker: TextSpeaker? = null
    override val notifier: Notifier? = null
    override val lightSensor: LightSensor? = null
    override val flashlight: Flashlight? = null
    override val frontSpeaker: FrontSpeaker? = null
}